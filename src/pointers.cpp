#include <iostream>
#include <string>
using namespace std;


int main()
{
  int x = 25, y = 2 * x;    
  auto a = &x, b = a;
  auto c = *a, d = 2 * *b;



	//variable data types
	cout << a << endl;
	cout << "variable a is storing the memory address of x" << endl;
	
	cout << endl;
	
	cout << b << endl;
	cout << "variable a is storing 10FA1, so b is equal to 10FA1 also" << endl;
	
	cout << endl;
	
	cout << c << endl;
	cout << "c is equal to pointer a and a is pointing at the memory address. 10FA1 has 25 in it so c is equal to 25" << endl;
	
	cout << endl;
	
	cout << d << endl;
	cout << "d is equal to 2 multiply by pointer b which is pointing at a which the memory address 10FA1 which is 25*2 so d is equal to 50" << endl;
	
	cout << endl;
	
  cout<<"x = "<<x<<", y = "<<y<<endl;
  
  cout << "Print a" << endl;
	cout << a << endl;
	
	cout << "Print b" << endl;
		cout << b << endl;
		
	cout << "Print c" << endl;
		cout << c << endl;
		
	cout << "print d" << endl;
		cout << d << endl;

 



}
